# mlops-tutorial

[Train them and they will leave!](https://www.inroads.co.in/blogs/2015/3/6/train-them-and-they-will-leave)

## Week 1

**Topic**

- Develop a model for choosen data on a jupyter/colab etc. notebook.
- Convert preprocessing code into module.

**To-Dos for all until next week**
- Add label_encoder to preprocessing module
- Add another scaler to preprocessing module

## Week 2 - 20.01.2023

**Topic**

- Use venv for local development
- Convert model codes into modules. see
  [example](https://towardsdatascience.com/3-ways-to-deploy-machine-learning-models-in-production-cdba15b00e)

### Assignments:

- Categorical Value Encoding : Ahmet

## Week 3

**Topic**

- Model save-load and inference modules

**To-Dos for all until next week**

- Generate model-load and prediction module module
- Add label_encoder to preprocessing module
- Add another scaler to preprocessing module

#### Assignments :

- Numerical Value Scaling : Halil -10dk
- Convert problem to classification using feature discretization : Cabir max 5dk
- Model Evaluation : regression (mse, rmsq, r2), classification(f1, recall, precision, auc, roc curve) : Gursev 15 dk
- Hashing encoding and Binary encoding implementation using our dataset : Ahmet max 5dk
- Model Explainability: Feature Importance using SHAP : Bunyamin
- Feature Crossing : Aydin


## Upcoming Tasks

- Logging
- Linting
- Unit testing
- CI/CD Pipeline
  -[CI/CD tutorial](https://www.youtube.com/watch?v=HGJWMTNeYqI&list=RDCMUCUUl_HXJjU--iYjUkIgEcTw&index=7&ab_channel=ValentinDespa)
- API dev and deployment
- Containerization
- Performance Testing
- Code Optimization
- Monitoring and alerting (theoretically)
- Kubernetes integration (theoretically)
- MLflow-Prefect-BentoML

## Resources
### Repos

[MLOps-ZoomCamp](https://github.com/DataTalksClub/mlops-zoomcamp)

### Articles

[Medium - Good Data Scientists Write Good Code](https://towardsdatascience.com/good-data-scientists-write-good-code-28352a826d1f)

### Websites

[NeptuneAI-MLOps Blog](https://neptune.ai/blog)
[Machine Learning Mastery](https://machinelearningmastery.com/)

### Courses
[madewithml](https://madewithml.com/#mlops)


### Books
[Designing Machine Learning Systems](https://www.oreilly.com/library/view/designing-machine-learning/9781098107956/)