# task_manager
import pandas as pd
import numpy as np
import datetime as dt
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
# import lightgbm as ltb
import warnings
warnings.filterwarnings("ignore")


class PrePocessManager:
    """_summary_
    """

    def __init__(self, data_path, categorical_columns, numerical_columns):
        self.data_path = data_path
        self.categorical_columns = categorical_columns
        self.numerical_columns = numerical_columns
        self.scaler = MinMaxScaler()

    def get_data(self):
        """_summary_

        Returns:
            _type_: _description_
        """

        data = pd.read_csv(self.data_path)
        data.columns = data.columns.str.lower()
        df_raw = data.sample(frac=0.05)
        data.drop_duplicates(keep="first", inplace=True)
        self.df_raw = df_raw.copy()
        return df_raw

    def get_new_features(self):
        """_summary_

        Returns:
            _type_: _description_
        """
        df_features = self.get_data()
        df_features['date'] = pd.to_datetime(df_features['date'])
        df_features['year'] = df_features['date'].dt.year
        df_features['month'] = df_features['date'].dt.month
        df_features['day'] = df_features['date'].dt.day
        df_features['week'] = df_features['date'].dt.week
        df_features['weekend'] = df_features['week'].apply(lambda x: 1 if x in [6,7] else 0)
        df_features['day_of_week'] = df_features['date'].dt.dayofweek
        df_features['quarter'] = df_features['date'].dt.quarter
        df_features = df_features.drop(["id","date"], axis=1)
        ##change sales column index
        df_features.insert(0, '#order', df_features.pop("#order"))
        ##rename sales as target_variable
        df_features.columns = df_features.columns.str.replace('#order', 'target_variable')

        return df_features

    def add_encode_categoricals(self):
        """_summary_

        Returns:
            _type_: _description_
        """
        df = self.get_new_features()
        df_dummies_1 = pd.get_dummies(data=df, columns=self.categorical_columns)
        return df_dummies_1

    def add_normalize_numericals(self):
        """_summary_
        Returns:
            _type_: _description_
        """
        df_dummies_2 = self.add_encode_categoricals()
        # add sales column to dummies_df
        df_dummies_2[self.numerical_columns] = self.df_raw[self.numerical_columns].values
        # scale sales column
        df_dummies_2[self.numerical_columns] = self.scaler.fit_transform(df_dummies_2[self.numerical_columns])
        return df_dummies_2

    def run(self):
        return self.add_normalize_numericals()


class ModelManager:

    def __init__(self, dataset):
        self.dataset = dataset
        self.train_test_split()

    def train_test_split(self):
        X = self.dataset.iloc[:, 2:]
        y = self.dataset.iloc[:, 0]
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(X, y, test_size=0.2, random_state=42)
        print("Dataset Splitted to train and test sets")

    def train_model(self, model_name):
        self.model_name = model_name
        if model_name == "logistic_regression":
            print(f"Training {model_name} model started...")
            self.model = LinearRegression().fit(self.X_train, self.y_train)
        elif model_name == "random_forest":
            print(f"Training {model_name} model started...")
            self.model = RandomForestRegressor(n_estimators = 300, max_features = 'sqrt', max_depth = 5, random_state = 18).fit(self.X_train, self.y_train)
        print("Training model finished...")

    def get_metrics(self):
        y_pred = self.model.predict(self.X_test)
         # Calculate MSE
        mse = mean_squared_error(self.y_test, y_pred)
        # Calculate RMSE
        rmse = np.sqrt(mean_squared_error(self.y_test, y_pred))
        # Calculate r2_score
        r2 = r2_score(self.y_test, y_pred)
        # Print the results
        print(f"Results for {self.model_name}")
        print("Mean Squared Error: ", mse)
        print("Root Mean Squared Error: ", rmse)
        print("r2_score: ", r2)






