import unittest
import pandas as pd
from task_manager import PrePocessManager

class TestPreProcessManager(unittest.TestCase):

    def setUp(self):
        self.data_path = 'path/to/data.csv'
        self.categorical_columns = ['category', 'location']
        self.numerical_columns = ['#order']
        self.manager = PrePocessManager(self.data_path, self.categorical_columns, self.numerical_columns)

    def test_get_data(self):
        data = self.manager.get_data()
        self.assertIsInstance(data, pd.DataFrame)
        self.assertEqual(data.shape[1], 4)

    def test_get_new_features(self):
        data = self.manager.get_new_features()
        self.assertIsInstance(data, pd.DataFrame)
        self.assertEqual(data.shape[1], 10)
        self.assertTrue('year' in data.columns)
        self.assertTrue('month' in data.columns)
        self.assertTrue('day' in data.columns)
        self.assertTrue('week' in data.columns)
        self.assertTrue('day_of_week' in data.columns)
        self.assertTrue('quarter' in data.columns)

    def test_add_encode_categoricals(self):
        data = self.manager.add_encode_categoricals()
        self.assertIsInstance(data, pd.DataFrame)
        self.assertEqual(data.shape[1], 12)
        self.assertTrue('category_A' in data.columns)
        self.assertTrue('category_B' in data.columns)
        self.assertTrue('location_X' in data.columns)
        self.assertTrue('location_Y' in data.columns)

    def test_add_normalize_numericals(self):
        data = self.manager.add_normalize_numericals()
        self.assertIsinstance(data, pd.DataFrame)
        self.assertEqual(data.shape[1], 13)
        self.assertTrue('#order' in data.columns)
        self.assertTrue((data['#order'] >= 0).all())
        self.assertTrue((data['#order'] <= 1).all())

    def test_run(self):
        data = self.manager.run()
        self.assertIsinstance(data, pd.DataFrame)
        self.assertEqual(data.shape[1], 13)
        self.assertTrue('year' in data.columns)
        self.assertTrue('month' in data.columns)
        self.assertTrue('day' in data.columns)
        self.assertTrue('week' in data.columns)
        self.assertTrue('day_of_week' in data.columns)
        self.assertTrue('quarter' in data.column
